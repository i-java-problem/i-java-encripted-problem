/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i.java.encripted.problem.lliure.server;

import i.java.encripted.problem.message.Message;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author Kirasumairu
 */
public class ServerOptions extends javax.swing.JFrame {

    private ServerOptions me;
    private Socket socket;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private CheckMessage check;

    /**
     * Creates new form ServerOptions
     */
    public ServerOptions(Socket socket) {
        initComponents();
        this.setVisible(true);
        this.setTitle("Panell client -> " + socket.getInetAddress() + ":" + socket.getPort());
        this.socket = socket;
        check = new CheckMessage();
        me = this;
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        (new Thread(check)).start();
        (new Thread(new CheckVisible())).start();
    }

    //Checks if the window has been closed
    class CheckVisible implements Runnable {

        @Override
        public void run() {
            while (me.isVisible()) {
                try {
                    Thread.sleep(300);
                } catch (Exception ex) {
                }
            }
            check.disconnect();
        }
    }

    class CheckMessage implements Runnable {

        private volatile boolean connected = true;

        private void disconnect() {
            try {
                connected = false;
                if (me.isVisible()) {
                    me.setVisible(false);
                }
                socket.close();
            } catch (Exception ex) {
            }
        }

        @Override
        public void run() {
            try {
                oos = new ObjectOutputStream(socket.getOutputStream());
                ois = new ObjectInputStream(socket.getInputStream());
            } catch (IOException ex) {
            }
            while (connected) {
                try {
                    Message message = (Message) ois.readObject();
                    if (message.getType() == Message.Type.DISCONNECTION) {
                        disconnect();
                    } else if (message.getType() == Message.Type.MESSAGE) {
                        //TODO IMPLEMENT MESSAGE CONTENT
                    }
                } catch (Exception ex) {
                    disconnect();
                }
            }
            try {
                oos.close();
                ois.close();
            } catch (Exception ex) {
            }
        }
    }

    private void hash() {
        try {
            String missatge = textFieldMissatge.getText();
            ObjectOutputStream mos = new ObjectOutputStream(new FileOutputStream(textFieldOutput.getText()));
            mos.writeObject(missatge.getBytes());
            mos.close();
            oos.writeObject(new Message(Message.Type.PETICIO, ""));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ServerOptions.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ServerOptions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        textFieldMissatge = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textFieldOutput = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Century", 1, 14)); // NOI18N
        jLabel1.setText("Xifrar amb hash");

        textFieldMissatge.setText("missatge secret");

        jLabel2.setText("Text");

        textFieldOutput.setText("arxiu.dat");

        jLabel3.setText("Output");

        jButton1.setText("Enviar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(textFieldMissatge))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(textFieldOutput, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldMissatge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addContainerGap(350, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        hash();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField textFieldMissatge;
    private javax.swing.JTextField textFieldOutput;
    // End of variables declaration//GEN-END:variables
}
