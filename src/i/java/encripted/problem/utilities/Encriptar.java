/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i.java.encripted.problem.utilities;

import java.util.Arrays;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Marc
 */
 public class Encriptar {
    private SecretKey key;       
    private String algoritme= "AES";
    private int keysize=16;

    public SecretKey getKey() {
        return key;
    }

    //Generem una key a partir d'un String proporcionat
public void addKey( String value ){
        byte[] valuebytes = value.getBytes();            
        key = new SecretKeySpec( Arrays.copyOf( valuebytes, keysize ) , algoritme );      
    }

}