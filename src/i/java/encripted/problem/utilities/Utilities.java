/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i.java.encripted.problem.utilities;

import i.java.encripted.problem.IJavaEncriptedProblem;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Kirasumairu
 */
public class Utilities {
    
    public static void GeneracioHash() {
        FileOutputStream fitxernormal = null;

        System.out.println("Generacio i emmagatzematge de Hash");

        try {
            //Generar fitxers missatge i missatgehash
            Scanner lector = new Scanner(System.in);
            System.out.println("Introdueix el nom del fitxer on s'emmagatzemara el missatge. Per exemple missatge.dat");
            String nomfitxer = lector.nextLine();
            fitxernormal = new FileOutputStream("hash/" + nomfitxer);
            System.out.println("Introdueix el nom del fitxer on s'emmagatzemara el Hash. Per exemple missatgehash.dat");
            String nomfitxerxifrat = lector.nextLine();
            FileOutputStream fitxerhash = new FileOutputStream("hash/" + nomfitxerxifrat);

            //Escriure el missatge al fitxer normal
            System.out.println("Introdueix el missatge. Per exemple Hola");
            String missatge = lector.nextLine();

            ObjectOutputStream out = new ObjectOutputStream(fitxernormal);
            out.writeObject(missatge);

            System.out.println("El teu missatge és: " + missatge);
            System.out.println("bytes:" + missatge.getBytes());

            //Generar el hash del missatge
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(missatge.getBytes());

            byte resumActual[] = md.digest();

            System.out.print("El hash (SHA-512): ");
            String valorhash = bytesToHex(resumActual);
            System.out.println(valorhash);

            //Escriure el hash al fitxerhash
            ObjectOutputStream escriurehash = new ObjectOutputStream(fitxerhash);
            escriurehash.writeObject(valorhash);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //Converteix array de bytes a hexadecimal
    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static void ComprobacioHashI() {
        FileInputStream fileIn = null;
        Scanner lector = new Scanner(System.in);
        try {
            System.out.println("Comprobacio Hash I");

            System.out.println("Introdueix el nom del fitxer que emmagatzema el missatge.");
            String nomfitxer = lector.nextLine();

            //Llegim el valor del fitxer:
            fileIn = new FileInputStream("hash/" + nomfitxer);
            ObjectInputStream dataIS = new ObjectInputStream(fileIn);
            // Obtenim la cadena
            Object o = dataIS.readObject();
            String dades = (String) o;

            //Generar hash
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(dades.getBytes());

            byte resumActual[] = md.digest();

            //Convertim el valor del array de bytes del fitxer a hexadecimal per a poder-lo comparar amb el String del hash introduit manualment
            String valorhash = bytesToHex(resumActual);

            //Mostrem el Hash correcte que volem comparar manualment després
            System.out.println("hash:" + valorhash);

            System.out.println("Introdueix el Hash manualment:");
            String hashmanual = lector.nextLine();

            if (hashmanual.equals(valorhash)) {
                System.out.println("El missatge no ha estat alterat!");
                System.out.println("El missatge del fitxer es: " + dades);
            } else {
                System.out.println("El missatge ha estat alterat!");
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileIn.close();
            } catch (IOException ex) {
                Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void ComprobacioHashII() {

        FileInputStream fileIn = null;
        FileInputStream fileHash = null;
        Scanner lector = new Scanner(System.in);
        try {
            System.out.println("Comprobacio Hash II");

            System.out.println("Introdueix el nom del fitxer que emmagatzema el missatge.");
            String nomfitxer = lector.nextLine();

            //Llegim el valor del fitxer:
            fileIn = new FileInputStream("hash/" + nomfitxer);
            ObjectInputStream dataIS = new ObjectInputStream(fileIn);
            // Obtenim la cadena
            Object o = dataIS.readObject();
            String dades = (String) o;

            //Generar hash
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(dades.getBytes());

            byte resumActual[] = md.digest();

            //Convertim el valor del array de bytes del fitxer a hexadecimal per a poder-lo comparar amb el String del hash introduit manualment
            String valorhash = bytesToHex(resumActual);

            //Mostrem el Hash generat del fitxer actual que compararem després
            System.out.println("hash:" + valorhash);

            System.out.println("Introdueix el nom del fitxer que emmagatzema el hash.");
            String nomfitxerhash = lector.nextLine();
            //Llegim el valor del fitxer:
            fileHash = new FileInputStream("hash/" + nomfitxerhash);
            ObjectInputStream dataHash = new ObjectInputStream(fileHash);
            // Obtenim la cadena
            Object ohash = dataHash.readObject();
            String dadeshash = (String) ohash;
            //Hash del fitxer guardat en el apartat 1 quan hem creat els fitxers
            System.out.println("fitxer hash:" + dadeshash);

            if (valorhash.equals(dadeshash)) {
                System.out.println("El missatge no ha estat alterat!");
                System.out.println("El missatge del fitxer es: " + dades);
            } else {
                System.out.println("El missatge ha estat alterat!");
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileIn.close();
            } catch (IOException ex) {
                Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void XifrarFitxer() {
        FileOutputStream clau = null;
        FileOutputStream docxifrat = null;

        try {
            Encriptar encriptar = new Encriptar();
            Scanner lector = new Scanner(System.in);
            System.out.println("Introdueix el nom del fitxer que vols xifrar: (exemple: foto.jpg)");
            String fitxer = lector.nextLine();

            // Arxiu a xifrar
            FileInputStream filein = new FileInputStream("crypt/" + fitxer);

            System.out.println("Introdueix el password que vols utilitzar per a xifrar: (exemple: hola)");
            String password = lector.nextLine();
            //Metode per a generar una key a partir d'un String proporcionat
            encriptar.addKey(password);
            //Obtenim la key amb la nostra password
            SecretKey key = encriptar.getKey();

            //Guardem la key en un fitxer 
            clau = new FileOutputStream("crypt/Clau.secreta");
            ObjectOutputStream out = new ObjectOutputStream(clau);
            out.writeObject(key);
            out.close();

            docxifrat = new FileOutputStream("crypt/"+fitxer+".aes");

            // Cipher per encriptar
            Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
            c.init(Cipher.ENCRYPT_MODE, key);

            //Objecte CipherOutputStream on es desa l'arxiu xifrat
            CipherOutputStream cip = new CipherOutputStream(docxifrat, c);
            int midaBloc = c.getBlockSize();//tamany de bloc de l'objecte Cipher
            byte[] bytes = new byte[midaBloc];//bloc de bytes

            // Llegim els bytes de l'arxiu PDF
            // i s'escriu a CipherOutputStream
            int i = filein.read(bytes);
            while (i != -1) {
                cip.write(bytes, 0, i);
                i = filein.read(bytes);
            }
            cip.flush();
            cip.close();
            filein.close();
            System.out.println("Arxiu xifrat amb clau secreta.");

        } catch (FileNotFoundException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    public static void DesxifrarFitxer() {
        try {

            ObjectInputStream oin = null;
            Encriptar encriptar = new Encriptar();
            Scanner lector = new Scanner(System.in);
            System.out.println("Desxifrar fitxer");

            System.out.println("Introdueix el nom del fitxer que vols desxifrar: (exemple: foto.jpg.aes)");
            String fitxerdesxifrar = lector.nextLine();

            System.out.println("Introdueix el password que vols utilitzar per a xifrar: (exemple: hola)");
            String password = lector.nextLine();
            //Generem la mateixa password amb la que s'ha xifrat el fitxer
            encriptar.addKey(password);
            SecretKey key = encriptar.getKey();
            
            //Agafem la clau que tenim guardada i que es la correcta
            FileInputStream savekey = new FileInputStream("crypt/Clau.secreta");
            ObjectInputStream secretkey = new ObjectInputStream(savekey);
            Key clauSecreta = (Key)secretkey.readObject();
            secretkey.close();
            
//            System.out.println("clau nova:"+key);
//            System.out.println("clau guardada:"+clauSecreta);
            
            //Comparem la clau introduida per el usuari amb la clau que tenim guardada
            if (clauSecreta.equals(key)) {
                
            

            // Es defineix l'objecte Cipher per desencriptar
            Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE, key);

            //Objecte cipherInputStream el contingut del qual es desxifrarà
            CipherInputStream in = new CipherInputStream(
                    new FileInputStream("crypt/" + fitxerdesxifrar), c);
            int tambloque = c.getBlockSize();
            byte[] bytes = new byte[tambloque];
            tambloque = c.getBlockSize();//mida del bloc
            bytes = new byte[tambloque];//bloc de bytes

            // Arxiu amb el contingut desxifrat que es crearà
            FileOutputStream fileout = new FileOutputStream("crypt/"+fitxerdesxifrar+".clar");

            // LLegim blocs de bytes de l'arxiu xifrat
            // Els anem escrivint desencriptats al FileOutputStream
            int i = in.read(bytes);
            while (i != -1) {
                fileout.write(bytes, 0, i);
                i = in.read(bytes);
            }
            fileout.close();
            in.close();

            System.out.println("Arxiu desxifrat amb clau secreta.");
            }else {
                System.out.println("La clau introduida per desxifrar no es correcta");
            }
            
        } catch (IOException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(IJavaEncriptedProblem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    //REQUERIMENT CIFRAT ASSIMETRIC
     public static void cifrarFichero() throws IOException{
         ObjectInputStream ois = null;
        Socket s = null;
        PublicKey claupub;
        ObjectOutputStream fo = null;
        ObjectOutputStream foo = null;
        

        try {
            //objectOutput para escribir en los archivos
            fo = new ObjectOutputStream(new FileOutputStream("EnigmaAesWrapRsa.pswd"));
            foo = new ObjectOutputStream(new FileOutputStream("enigma.jpg.aes"));
           
            String address = "127.0.0.1";
            s = new Socket(address, 12345);
            ois = new ObjectInputStream(s.getInputStream());
             

            KeyGenerator kg = KeyGenerator.getInstance("AES");
            kg.init(128);
            SecretKey clauSecreta = kg.generateKey();

            claupub = (PublicKey) ois.readObject();
          //  System.out.println(claupub.toString());

            //Encriptado la AES(secreta) con la RSA(pública del server)
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.WRAP_MODE, claupub);
            byte claveSecretaPerfecta[] = cipher.wrap(clauSecreta);
          //  System.out.println(new String(claveSecretaPerfecta));

            //Encriptado el texto con la AES(secreta)
            cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, clauSecreta);
            byte textPla[] = "m09 es la mejor asignatura del mundo".getBytes();
            byte textoCifrado[] = cipher.doFinal(textPla);
            
            fo.writeObject(claveSecretaPerfecta);
            fo.close();

            foo.writeObject(textoCifrado);
            foo.close();
            
            ObjectOutputStream dos = new ObjectOutputStream(s.getOutputStream());
            enviarArxiu(s, dos);
            
            
                
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ois != null) {
                ois.close();
            }
            if (s != null) {
                s.close();
            }
        }
    }
    
  public static void enviarArxiu(Socket socket, ObjectOutputStream dos) throws IOException {
      
        ArrayList<File> list = new ArrayList<>();
                list.add(new File("EnigmaAesWrapRsa.pswd"));
                list.add(new File("enigma.jpg.aes"));
                dos.writeInt(list.size());
                dos.flush();
                int n = 0;
                byte[] buffer = new byte[4092];
                for (File actual : list) {
                    dos.writeUTF(actual.getName());
                    dos.flush();
                    dos.writeLong(actual.length());
                    dos.flush();
                    FileInputStream fis = new FileInputStream(actual);
                    while ((n = fis.read(buffer)) != -1) {
                        dos.write(buffer, 0, n);
                        dos.flush();
                    }
                }
                dos.close();
            
        
        /*  try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            byte[] buffer = new byte[1024];
            int count;
            while ((count = arxiu.read(buffer))> 0) {
                oos.write(buffer, 0, count);
            }
            oos.flush();
            oos.close();
        } catch (IOException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }
    
    public static void rebreArxiu(Socket socket, BufferedOutputStream arxiu) {
        try {
            InputStream input = socket.getInputStream();
            byte[] buffer = new byte[1024];
            int count;
            while ((count = input.read(buffer)) > 0) {
                arxiu.write(buffer, 0, count);
            }
            arxiu.flush();
            arxiu.close();
        } catch (IOException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
