/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i.java.encripted.problem.message;

import java.io.Serializable;

/**
 *
 * @author Kirasumairu
 */
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    public enum Type {
        DISCONNECTION, MESSAGE, TRANSFERACCEPTED, TRANSFERDENIED, PETICIO, HASH
    }

    Type type;
    String message;

    public Message(Type type, String message) {
        this.type = type;
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

}
