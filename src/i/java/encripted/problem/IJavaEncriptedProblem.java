/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i.java.encripted.problem;

import i.java.encripted.problem.lliure.server.Server;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import i.java.encripted.problem.utilities.Utilities;
import static i.java.encripted.problem.utilities.Utilities.DesxifrarFitxer;
import static i.java.encripted.problem.utilities.Utilities.XifrarFitxer;

/**
 *
 * @authors Mario, Marc, Robin
 */

public class IJavaEncriptedProblem {
    
    private void start() {
        boolean continuar = true;
        while (continuar) {
            int requeriment = menu("1. REQUERIMENT 1 - Operacions amb funció Hash\n2. REQUERIMENT 2 - Xifrat simètric\n"
                    + "3. REQUERIMENTS DESITJATS\n4. Sortir\nIntrodueix una opció: ", new int[]{1, 2, 3, 4});
            switch (requeriment) {
                case 1:
                    int operacioHash = menu("1. Generació i emmagatzematge de Hash\n2. Comprobació de Hash: Introducció per teclat\n"
                            + "3. Comprobació de Hash II: Utilitzant un fitxer\n4. Sortir\nIntrodueix una opció: ", new int[]{1, 2, 3, 4});
                    switch (operacioHash) {
                        case 1:
                            Utilities.GeneracioHash();
                            break;
                        case 2:
                            Utilities.ComprobacioHashI();
                            break;
                        case 3:
                            Utilities.ComprobacioHashII();
                            break;
                    }
                    break;
                case 2:
                    int xifratSimetric = menu("1. Xifrar un fitxer (amb criptografia simètrica AES)\n2. Desxifrar un fitxer (amb criptografia simètrica AES)\n"
                            + "3. Sortir\nIntrodueix una opció: ", new int[]{1, 2, 3});
                    switch (xifratSimetric) {
                        case 1:
                            XifrarFitxer();
                            break;
                        case 2:
                            DesxifrarFitxer();
                            break;
                        case 3:
                            
                            break;
                    }
                    break;
                case 3:
                    int requerimentDesitjat = menu("1. Comunicació segura\n2. Signatura digital\n"
                            + "3. Lliure\n4. Sortir\nIntrodueix una opció: ", new int[]{1, 2, 3, 4});
                    switch (requerimentDesitjat) {
                        case 1:
                            
                            break;
                        case 2:
                            
                            break;
                        case 3:
                            new Server();
                            break;
                    }
                    break;
                case 4:
                    continuar = false;
                    break;
            }
        }
    }

    private int menu(String imprimir, int[] llista) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int num = -1;
        boolean correcte = false;
        while (!correcte) {
            System.out.print(imprimir);
            try {
                num = Integer.parseInt(reader.readLine());
                int i = 0;
                while (i < llista.length && !correcte) {
                    if (num == llista[i]) {
                        correcte = true;
                    } else {
                        i++;
                    }
                }
                if (!correcte) {
                    System.out.println("Error: valor fora del rang");
                }
            } catch (Exception ex) {
                System.out.println("Error: valor no vàlid.");
            }
        }
        System.out.println("----------------------------------------------------");
        return num;
    }

    public static void main(String[] args) {
        (new IJavaEncriptedProblem()).start();
    }
}
